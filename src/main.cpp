#include "../include/corrida.hpp"

int main()
{
	int escolha;
	int continuar = 1;
	Corrida* c = new Corrida;

	vector<string> sI = c->getSaposInfo();
	vector<Sapo> run = c->getCorrida();
	vector<Sapo> podium = c->getClassificacao();

	while(continuar == 1)
	{
		c->guardarSapos(sI);

		cout << "O que você quer fazer? (Digite o número correspondente)" << endl; 
		cout << "1 - Ver Sapos, 2 - Ver Pistas, 3 - Criar Sapo, 4 - Criar Pista, 5 - Criar Corrida" << endl;
		cin >> escolha;

		switch(escolha)
		{
			case 1:
				c->verSapos(sI);
				break;

			case 2:
				c->verPistas();
				break;

			case 3:
				c->criarSapo();
				break;

			case 4:
				c->criarPista();
				break;

			case 5:
				c->iniciarCorrida(sI, run, podium);
				break;

			default:
				cout << "Opção Inválida" << endl;
		}

		cout << "Quer fazer mais alguma coisa? (1 - Sim, 2 - Não)" << endl;
		cin >> continuar;
	}

	delete c;
	
	return 0;
}