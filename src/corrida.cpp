#include "../include/corrida.hpp"

vector<string> Corrida::getSaposInfo()
{
	return saposInfo;
}

vector<Sapo> Corrida::getCorrida()
{
	return corrida;
}

vector<Sapo> Corrida::getClassificacao()
{
	return classificacao;
}

void Corrida::criarCorrida(vector<string> &saposInfo, vector<Sapo> &corrida)
{
	int num;
	string s, line;
	Pista pista;
	Sapo sapo;

	cout << "Digite (sem espaços) o nome da pista a ser disputada a corrida: " << endl;
	cin >> s;

	inFile2.open("../corridaDeSapos2.0/info/pistas.txt");

	if(!inFile2)
	{
		cout << "Não foi possível abrir o arquivo" << endl;
		abort();
	}

	int count = 1;
	while(!inFile2.eof() && count <= 3)
	{
		getline(inFile2, line);

		if(s == line && count == 1)
		{
			pista.setNome(line);
			count++;
		}

		else if(count == 2)
		{
			stringstream strstr;
			strstr << line;		//Pega a primeira palavra da linha (no caso, o número)
			strstr >> num;		//Através do operador >>, strstr converte o valor automaticamente para int
			pista.setNumSapos(num);
			count++;
		}

		else if(count == 3)
		{
			stringstream strstr;
			strstr << line;		//Idem para o resto
			strstr >> num;
			pista.setDist(num);
			count++;
		}
	}

	inFile2.close();

	num = pista.getDist();
	Sapo::setDistTotal(num);

	int numS = pista.getNumSapos();

	for(int i = 0; i < numS; i++)
	{
		cout << "Digite (sem espaços) o nome do sapo que irá disputar a corrida: " << endl;
		cin >> s;

		int sizeI = saposInfo.size();

		int numLine = 0;
		int count = 1;
		while(numLine < sizeI && count <= 6)
		{
			line = saposInfo[numLine];

			if(s == line && count == 1)
			{
				sapo.setNome(line);
				count++;
			}

			else if(count == 2)
			{
				stringstream strstr;
				strstr << line;
				strstr >> num;
				sapo.setId(num);
				count++;
			}

			else if(count == 3)
			{
				stringstream strstr;
				strstr << line;
				strstr >> num;
				sapo.setTotalDisputas(num);
				count++;
			}

			else if(count == 4)
			{
				stringstream strstr;
				strstr << line;
				strstr >> num;
				sapo.setVitorias(num);
				count++;
			}

			else if(count == 5)
			{
				stringstream strstr;
				strstr << line;
				strstr >> num;
				sapo.setEmpates(num);
				count++;
			}

			else if(count == 6)
			{
				stringstream strstr;
				strstr << line;
				strstr >> num;
				sapo.setTotalPulos(num);
				count++;
			}

			numLine++;
		}

		corrida.push_back(sapo);
	}
}

void Corrida::guardarSapos(vector<string> &saposInfo)
{
	int sizeI = saposInfo.size();
	string s;

	if(sizeI > 0)
	{
		for(int i = 0; i < sizeI; i++)
		{
			saposInfo.erase(saposInfo.begin() + i);
		}
	}

	inFile2.open("../corridaDeSapos2.0/info/sapos.txt");

	if(!inFile2)
	{
		cout << "Não foi possível abrir o arquivo" << endl;
		abort();
	}

	while(!inFile2.eof())
	{
		getline(inFile2, s);
		saposInfo.push_back(s);
	}

	inFile2.close();
}

void Corrida::criarSapo()
{
	string s;
	outFile2.open("../corridaDeSapos2.0/info/sapos.txt", ios::app);
	
	if(!outFile2)
	{
		cout << "Não foi possível abrir o arquivo" << endl;
		abort();
	}

	cout << "Digite (sem espaços), o (a):" << "\n" << "Nome do sapo:" << endl;
	cin >> s;
	outFile2 << "\n" << s << endl;

	cout << "ID do sapo:" << endl;
	cin >> s;
	outFile2 << s << " : ID" << endl;

	outFile2 << "0 : Provas disputadas" << "\n" << "0 : Vitórias" << "\n" << 
			    "0 : Empates" << "\n" << "0 : Total de pulos dados" << endl;

	outFile2.close();
}

void Corrida::criarPista()
{
	string s;
	outFile2.open("../corridaDeSapos2.0/info/pistas.txt", ios::app);

	if(!outFile2)
	{
		cout << "Não foi possível abrir o arquivo" << endl;
		abort();
	}

	cout << "Digite o (a):" << "\n" << "Nome da pista:" << endl;
	cin >> s;
	outFile2 << "\n" << s << endl;

	cout << "Número de sapos a competir:" << endl;
	cin >> s;
	outFile2 << s << " : Número de sapos" << endl;

	cout << "Distância total da corrida:" << endl;
	cin >> s;
	outFile2 << s << " : Distância total" << endl;

	outFile2.close();
}

void Corrida::verSapos(vector<string> &saposInfo)
{
	int sizeI = saposInfo.size();
	for(int i = 0; i < sizeI; i++)
	{
		cout << saposInfo[i] << endl;
	}
}

void Corrida::verPistas()
{
	inFile2.open("../corridaDeSapos2.0/info/pistas.txt");
	string s;

	if(!inFile2)
	{
		cout << "Não foi possível abrir o arquivo" << endl;
		abort();
	}

	while(!inFile2.eof())
	{
		getline(inFile2, s);
		cout << s << endl;
	}

	inFile2.close();
}

void Corrida::iniciarCorrida(vector<string> &saposInfo, vector<Sapo> &corrida, vector<Sapo> &classificacao)
{
	criarCorrida(saposInfo, corrida);

	int raias = corrida.size();
	int distT = Sapo::getDistTotal();
	int stats;	// Vitórias/empates/total de pulos
	int sizeI = saposInfo.size();
	string start;

	for(int i = 0; i < raias; i++)
	{
		cout << "\nSAPO " << i << ": "  << corrida[i].getNome() << "\n" << corrida[i].getId() << " : ID\n"
			 << corrida[i].getTotalDisputas() << " : Provas disputadas\n" << corrida[i].getVitorias() 
			 << " : Vitórias\n" << corrida[i].getEmpates() << " : Empates\n" 
			 << corrida[i].getTotalPulos() << " : Total de pulos dados" << endl;
	}

	cout << "Aperte qualquer tecla, em seguida ENTER, para iniciar a corrida" << endl;
	cin >> start;

	int pos = 1;

	while(raias > 0)
	{
		int syncRaias = raias;
		
		for(int i = 0; i < syncRaias; i++)
		{
			corrida[i].pular();
			int distP = corrida[i].getDistPerc();

			if(distP >= distT)
			{		
				//Altera o rank, provas disputadas, total de pulos dados do sapo 
				//e o põe na tabela de classificação 
				corrida[i].setRank(pos);

				int pulos = corrida[i].getQtdPulos();
				
				stats = corrida[i].getTotalPulos();
				stats = stats + pulos;
				corrida[i].setTotalPulos(stats);

				stats = corrida[i].getTotalDisputas();
				stats++;
				corrida[i].setTotalDisputas(stats);

				classificacao.push_back(corrida[i]);

				//Tira o sapo da corrida
				corrida.erase(corrida.begin() + i);
				syncRaias--;				
			}
		}

		if(raias > syncRaias)
		{
			raias = syncRaias;
			pos++;
		}
	}

	//Verifica quantos sapos finalizaram a corrida ao mesmo tempo
	int numC = classificacao.size();
	int vencedores = 0;

	cout << "" << endl;
	for(int i = 0; i < numC; i++)
	{
		int r = classificacao[i].getRank();
		string n = classificacao[i].getNome();
		
		cout << r << "º lugar: " << n << endl;

		if(r == 1)
		{
			vencedores++;
		}
	}

	//Se só um finalizou, altera o número de vitórias do mesmo 
	if(vencedores == 1)
	{
		int v = classificacao[0].getVitorias();
		v++;
		classificacao[0].setVitorias(v);
	}

	//Se mais de um finalizou, altera o número de empates deles
	else
	{
		for(int i = 0; i < vencedores; i++)
		{
			int e = classificacao[i].getEmpates();
			e++;
			classificacao[i].setEmpates(e);
		}
	}

	vector<string> resultados;

	string endL = "\n";
	resultados.push_back(endL);

	//Pega, um de cada vez, cada sapo que participou da corrida
	for(int i = 0; i < numC; i++)
	{
		string nSapo = classificacao[i].getNome();

		int j = 0;
		int count = 1;

		//e modifica as informações contidas em saposInfo
		while(j < sizeI && count <= 6)
		{
			string aux;
			string line = saposInfo[j];

			//nome
			if(nSapo == line && count == 1)
			{
				resultados.push_back(line);
				
				count++;
			}

			else if(count == 2)
			{
				stringstream strstr;
				stats = classificacao[i].getId();

				strstr << stats;
				aux = strstr.str() + " : ID";

				resultados.push_back(aux);

				count++;
			}

			//provas disputadas
			else if(count == 3)
			{
				stringstream strstr;
				stats = classificacao[i].getTotalDisputas();

				strstr << stats;
				aux = strstr.str() + " : Provas disputadas";

				resultados.push_back(aux);
				
				count++;
			}
			
			//vitórias
			else if(count == 4)
			{
				stringstream strstr;
				stats = classificacao[i].getVitorias();

				strstr << stats;
				aux = strstr.str() + " : Vitórias";

				resultados.push_back(aux);
				
				count++;
			}
			
			//empates
			else if(count == 5)
			{
				stringstream strstr;
				stats = classificacao[i].getEmpates();

				strstr << stats;
				aux = strstr.str() + " : Empates";
				
				resultados.push_back(aux);

				count++;
			}
			//total de pulos dados
			else if(count == 6)
			{
				stringstream strstr;
				stats = classificacao[i].getTotalPulos();
				
				strstr << stats;
				aux = strstr.str() + " : Total de pulos dados\n";
				
				resultados.push_back(aux);
				
				count++;
			}

			j++;
		}
	}

	int sizeR = resultados.size();

	//Mostra as estatísticas dos sapos que participaram da corrida
	for(int i = 0; i < sizeR; i++)
	{
		cout << resultados[i] << endl;
	}
/*
	//Por fim, transfere as informações de resultados para sapos.txt
	outFile2.open("../corridaDeSapos2.0/info/sapos.txt");

	for(int i = 0; i < sizeR; i++)
	{
		string line = resultados[i];

		outFile2 << line << endl;
	}

	outFile2.close();
*/
}