#include "../include/sapo.hpp"

int Sapo::distTotal;

random_device Sapo::rand{};
mt19937 Sapo::seed(Sapo::rand());
uniform_int_distribution<> Sapo::d(1, 5);

Sapo::Sapo()
{
	nome = "NoName";
	id = 0;
	distPerc = 0;
	qtdPulos = 0;
	totalDisputas = 0;
	vitorias = 0;
	empates = 0;
	totalPulos = 0;
	rank = 0;
	distTotal = 0;
}

string Sapo::getNome()
{
	return nome;
}

void Sapo::setNome(string nome)
{
	this->nome = nome;
}

int Sapo::getId()
{
	return id;
}

void Sapo::setId(int id)
{
	this->id = id;
}

int Sapo::getDistPerc()
{
	return distPerc;
}

int Sapo::getQtdPulos()
{
	return qtdPulos;
}

void Sapo::setQtdPulos(int qtdPulos)
{
	this->qtdPulos = qtdPulos;
}

int Sapo::getTotalDisputas()
{
	return totalDisputas;
}

void Sapo::setTotalDisputas(int totalDisputas)
{
	this->totalDisputas = totalDisputas;
}

int Sapo::getVitorias()
{
	return vitorias;
}

void Sapo::setVitorias(int vitorias)
{
	this->vitorias = vitorias;
}

int Sapo::getEmpates()
{
	return empates;
}

void Sapo::setEmpates(int empates)
{
	this->empates = empates;
}

int Sapo::getTotalPulos()
{
	return totalPulos;
}

void Sapo::setTotalPulos(int totalPulos)
{
	this->totalPulos = totalPulos;
}

int Sapo::getRank()
{
	return rank;
}

void Sapo::setRank(int rank)
{
	this->rank = rank;
}

void Sapo::pular()
{
	int pulo;
	
	pulo = round(d(seed));
	distPerc = distPerc + pulo;
	qtdPulos++;

	cout << nome << ", ID: " << id << " pulou uma distância de " << pulo << "." << endl;
}

//Métodos estáticos
int Sapo::getDistTotal()
{
	return distTotal;
}

void Sapo::setDistTotal(int dT)
{
	distTotal = dT;
}
