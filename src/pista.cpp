#include "../include/pista.hpp"

Pista::Pista()
{
	nome = "NoName";
	numSapos = 0;
	dist = 0;
}

string Pista::getNome()
{
	return nome;
}

void Pista::setNome(string nome)
{
	this->nome = nome;
}

int Pista::getNumSapos()
{
	return numSapos;
}

void Pista::setNumSapos(int numSapos)
{
	this->numSapos = numSapos;
}

int Pista::getDist()
{
	return dist;
}

void Pista::setDist(int dist)
{
	this->dist = dist;
}