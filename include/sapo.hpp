#ifndef SAPO
#define SAPO

#include <iostream>
#include <random>
#include <string>
using namespace std;

class Sapo
{		
	private:
		string nome;
		int id, distPerc, qtdPulos, totalDisputas, vitorias, empates, totalPulos, rank;
		static int distTotal;
		
		static random_device rand;	//rand
		static mt19937 seed;		//seed
		static uniform_int_distribution<> d;	//intervalo
	
	public:
		//Construtor
		Sapo();

		//Getters e setters
		string getNome();
		void setNome(string nome);
		int getId();
		void setId(int id);
		int getDistPerc();
		int getQtdPulos();
		void setQtdPulos(int qtdPulos);
		int getTotalDisputas();
		void setTotalDisputas(int totalDisputas);
		int getVitorias();
		void setVitorias(int vitorias);
		int getEmpates();
		void setEmpates(int empates);
		int getTotalPulos();
		void setTotalPulos(int totalPulos);
		int getRank();
		void setRank(int rank);
	
		//Incrementa distância percorrida de forma aleatória 
		//e aumenta o número de pulos em uma unidade
		void pular();

		//Métodos estáticos
		static int getDistTotal();
		static void setDistTotal(int dT);
};

#endif