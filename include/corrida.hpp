#ifndef CORRIDA
#define CORRIDA

#include <fstream>
#include <sstream>
#include "sapo.hpp"
#include "pista.hpp"

class Corrida
{
	private:
		ifstream inFile2;
		ofstream outFile2;
		
		vector<string> saposInfo;
		vector<Sapo> corrida;
		vector<Sapo> classificacao;

		//Método a ser usado em iniciarCorrida();
		void criarCorrida(vector<string> &saposInfo, vector<Sapo> &corrida);

	public:
		//Getters
		vector<string> getSaposInfo();
		vector<Sapo> getCorrida();
		vector<Sapo> getClassificacao();
		

		void guardarSapos(vector<string> &saposInfo);
		void criarSapo();
		void criarPista();
		void verSapos(vector<string> &saposInfo);
		void verPistas();
		void iniciarCorrida(vector<string> &saposInfo, vector<Sapo> &corrida, vector<Sapo> &classificacao);
};

#endif