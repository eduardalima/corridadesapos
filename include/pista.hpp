#ifndef PISTA
#define PISTA

#include <iostream>
#include <string>
using namespace std;

class Pista
{
	private:
		string nome;

		//Número de sapos a competir ; Distância da corrida
		int numSapos, dist;

	public:
		//Construtor
		Pista();

		//Getters e setters
		string getNome();
		void setNome(string nome);
		int getNumSapos();
		void setNumSapos(int numSapos);
		int getDist();
		void setDist(int dist);
};

#endif