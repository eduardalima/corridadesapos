Consegui fazer praticamente tudo. O único problema se deu na hora de mesclar os dados dos sapos atualizados com os dados dos sapos que não participaram da corrida.

Por isso, no final resolvi mostrar as estatísticas atualizadas dos participantes, sem alterar no arquivo nesse caso (apenas para mostrar que eles foram realmente atualizados). 

----------------------------------------------------------------------------------------------------------------

Repositório Git (link):
https://bitbucket.org/eduardalima/corridadesapos/src/master/

----------------------------------------------------------------------------------------------------------------

Para excutar o programa, você deve acessar pelo terminal a pasta "corridaDeSapos2.0", digitar make, e em seguida make run.

Para limpar os arquivos-objeto, digite make clean;