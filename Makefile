PROG = bin/corridaDeSapos2.0

CC = g++

CPPFLAGS = -O0 -g -Wall -pedantic -std=c++11

OBJS = main.o sapo.o pista.o corrida.o

$(PROG) : $(OBJS)
	$(CC) $(OBJS) -o $(PROG) 
	mv *.o build

main.o : include/corrida.hpp
	$(CC) $(CPPFLAGS) -c src/main.cpp

sapo.o : include/sapo.hpp
	$(CC) $(CPPFLAGS) -c src/sapo.cpp

pista.o : include/pista.hpp
	$(CC) $(CPPFLAGS) -c src/pista.cpp
	
corrida.o : include/corrida.hpp
	$(CC) $(CPPFLAGS) -c src/corrida.cpp

run :
	./$(PROG)

clean :
	rm -f $(PROG) build/*.o
